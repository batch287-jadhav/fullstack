// Create a mock database
let posts = [];

// Posts ID
let count = 1;

// Add Post Data

document.querySelector("#form-add-post").addEventListener('submit', (e) => {

	// Prevents the page from reloading also preventing the default behavior of our event
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});

	count++;

	console.log(posts);
	alert('Succesfully added!');
	showPosts(posts);

});

// Show Posts

const showPosts = (posts) => {

	let postEntries = "";

	posts.forEach((post) => {

		postEntries += `
			<div id = "post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	});

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}


// Edit Post
// function called editPost() that takes a single argument 'id'
const editPost = (id) => {

	// The function first uses the querySelector() method to get the element with the id and assigns its innerHTML property to the title variable same with the body
	// innerHTML - sets or returns HTML elements
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
}


// Update Post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

	// At each iteration, the code whether the id property of the current post element is equal to the value of the txt-edit-id input field in the form
	for(let i = 0; i< posts.length; i++){
		
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
				posts[i].title = document.querySelector('#txt-edit-title').value;
				posts[i].body = document.querySelector('#txt-edit-body').value;

				alert('Succesfully updated!');
				showPosts(posts);

				break;
		};		
	};
});


// Delete Post

const deletePost = (id) => {

	posts.splice(id-1, 1);	
	alert("Post deleted!")
	// console.log(posts);

	document.querySelector("#div-post-entries").remove();
};











