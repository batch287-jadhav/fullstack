// The "document" refers to the whole webpage
// The "querySelector" is used to select a specific object (HTML element) from the document (webpage)

const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#txt-full-name");

// Alternatively, we can use the getElement functions to retrieve the element 

	// document.getElementById
	// document.getElementByClassName
	// document.getElementByTagName


// whenever an user interacts with a web page, this action is considered as an event

// "addEventListener" is function that takes two arguments
	// String identifying the event (keyup)
	// function that the listner will execute once the specified event is triggered (event)

txtFirstName.addEventListener('keyup', (event) => {
				// "innerHTML" property sets or returns the HTML content
				spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener('keyup', (event) => {
				console.log("The button is already released")
				console.log(event);
				// The "event.target" contains the element where the event happened
				console.log(event.target);

				// the "event.target.value" gets the value of the input object (similar to the textFirstName.value)
				console.log(event.target.value);
})






















