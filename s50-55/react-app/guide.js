Session 50
	create a s50-s55 folder
		inside the folder run, npx create-react-app react-app
			Wait for the "Happy hacking" sign
		cd react-app
			touch guide.js
			npm start, to start the application
			remove the unecessary files:
				App.test.js
				index.css
				logo.svg
				reportWebVitals.js
			install bootstrap dependencies
				npm install react-bootstrap bootstrap
			create a components folder in src
				create AppNavbar.js
				create Banner.js
				create Highlights.js
				refactor App.css
			create a pages folder in src
				create Home.js
		Activity
			create CourseCard.js
Session 51
	create a data folder inside src
	create a Courses.js in pages
