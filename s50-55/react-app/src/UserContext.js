import React from 'react';

// Create a Context Object {makes data available globally}
// A context object as the name suggest is a data type of an object that can be used to store information that can be shared to other components within the application.
// Context object is a different approach in passing information between components and allows easier access.
const UserContext = React.createContext();


// the "provider" component allows other components to consume/use the context object and supply to neccessary information needed to the context object.
export const UserProvider = UserContext.Provider;
export default UserContext;






















