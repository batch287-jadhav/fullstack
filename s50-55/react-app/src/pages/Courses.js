// import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';
import { useState, useEffect } from 'react'

export default function Courses(){

	const [ courses, setCourses] = useState([]);
	// console.log(courseData);
	// console.log(courseData[0]);

	// const courses = courseData.map(course => {
				// return (
					// <CourseCard key={course.id} course={course} />
				// );
	// });

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active-courses`,)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course => {
				return (
					<CourseCard key={course._id} course={course} />
				)
			}))
		})
	}, [])

	return (
	 	<>
	 		{courses}
	 	</>
	);
};