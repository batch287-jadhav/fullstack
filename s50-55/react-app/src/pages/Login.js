import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import {useNavigate, Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login(props) {
	
	// Allows us to consume the User Context objects and its properties to use for validation
	const { user, setUser } = useContext(UserContext);

	// State hooks to store the values of the input fields
	const navigate = useNavigate();

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	// State to determine whether submit button is enabled or not
	const [ isActive, setIsActive] = useState(false);

	useEffect(() => {


		// Validation to enable submit buttion when all fields are populated and both paswords match.
		if (email !== '' && password !== '') {
			setIsActive(true);
		} else {

			setIsActive(false);
		}
	}, [email, password])


	const retrieveUserDetails = (token) => { 
		// fetch('http://localhost:4000/users/details' 
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, { 
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Global user state for validation accross the whole project 
			// Changes the global "user" to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})

	}

	function authentication(e) {
		e.preventDefault();

		// setPassword('');
		// setEmail('');

		// sets the email of the authenticated user in the local storage
		// localStorage.setItem('email', email);

		// Sets the global user state to have porperties obtained from the local storage
		// setUser({email: localStorage.getItem('email')});
		// alert("You are logged in!");

		// navigate("/");
		// fetch('http://localhost:4000/users/login'
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})
			} else {
				Swal.fire({
					title: 'Authentication failed!',
					icon: 'error',
					text: 'Please check your login details and try again!'
				})
			}

		setEmail('');
		setPassword('');
	})
	}


	return (

		(user.id !== null)? 
			<Navigate to="/courses" />
		:

			<Form onSubmit={(e) => authentication(e)}>

			{/*<h1>Registration Form</h1>*/}
				<Form.Text className="text-dark fs-1 fw-bold">
						Login
				</Form.Text>

				<Form.Group controlId="email">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="password"
						value={password}
						onChange={e => setPassword(e.target.value)}
						required
					/>
				</Form.Group>

				{ isActive ?
					<Button variant="success" type='submit' id=' submitBtn'> 
						Login
					</Button>
					:
					<Button variant="danger" type='submit' id=' submitBtn' disabled> 
						Login
					</Button>
				}

			</Form>
	)
}
















