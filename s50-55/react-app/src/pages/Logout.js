import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';

export default function Logout(){

	// Consume the UserContext object and destructure it to access the user state and unsetUser funciton from the context provider
	const { unsetUser, setUser } = useContext(UserContext);

	// Remove the email from the local storage,
	// localStorage.clear();

	// Clear the local Storage of the users's infromation
	unsetUser();

	// Using useEffect, will allow Logout page to render first before triggering the useEffect which changes the state of user.
	useEffect(() => {
		setUser({id: null});
	})

	return(
		// after removing the email from the local storage it redirects us to login page
		<Navigate to="/login" />
	)
}























