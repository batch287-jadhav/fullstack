// console.log("Hello World");

// Get Post Data

fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json().then((data) => showPosts(data)));



// Add Post Data

document.querySelector("#form-add-post").addEventListener('submit', (e) => {

	// Prevents the page from reloading also preventing the default behavior of our event
	e.preventDefault();

	// The link above is of a placeholder which acts like a mock database 
	fetch('https://jsonplaceholder.typicode.com/posts', {

			method: 'POST',
			body: JSON.stringify({
				title: document.querySelector('#txt-title').value,
				body: document.querySelector('#txt-body').value,
				userId: 1
			}),

			headers: { 'content-type' : 'application/json; charset=UTF-8'}

	}).then((response) => response.json()).then((data) => {

			console.log(data);
			alert('Succesfully added!');

			// Mini-Activity
				// add another code, that will empty our input type, both title & body after the alert is done runnig. So that, after the first add post request it will be empty input.

			document.querySelector("#txt-title").value = null;
			document.querySelector("#txt-body").value = null; 
			// we can also use empty strings here ('')
	});

	// posts.push({
	// 	id: count,
	// 	title: document.querySelector('#txt-title').value,
	// 	body: document.querySelector('#txt-body').value
	// });

	// count++;

	// console.log(posts);
	// alert('Succesfully added!');
	// showPosts(posts);

});



// Show Posts

const showPosts = (posts) => {

	let postEntries = '';

	posts.forEach((post) => {

		postEntries += `
			<div id = "post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;

		document.querySelector('#div-post-entries').innerHTML = postEntries;
	});
};

// Edit post

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
};

// Update Post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {

			method: 'PUT',
			body: JSON.stringify({
				id: document.querySelector('#txt-edit-id').value,
				title: document.querySelector('#txt-edit-title').value,
				body: document.querySelector('#txt-edit-body').value,
				userId: 1
			}),

			headers: { 'content-type' : 'application/json; charset=UTF-8'}

	}).then((response) => response.json()).then((data) => {

			console.log(data);
			alert('Succesfully updated!');

			// Mini-Activity
				// add another code, that will empty our input type, both title & body after the alert is done runnig. So that, after the first add post request it will be empty input.

			document.querySelector("#txt-edit-id").value = '';
			document.querySelector("#txt-edit-title").value = ''; 
			document.querySelector("#txt-edit-body").value = '';
			document.querySelector("#btn-submit-update").setAttribute('disabled', true);
			// we can also use empty strings here ('')
	});
});

const deletePost = (id) => {

	// posts.splice(id-1, 1);	
	// console.log(posts);

	fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((data) => {

		data.splice(id-1,1);
		alert("Post deleted!");

		document.querySelector(`#post-${id}`).remove();
	})

	
};

































